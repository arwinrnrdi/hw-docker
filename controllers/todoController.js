const {Todo} = require ("../models")

class todoController{

    static findAll = async (req, res, next) =>{
        const data = await Todo.findAll()
        res.status(200).json(data)
    }

    static findOne = async(req, res , next) =>{
        const {id} = req.params;
        const data = await Todo.findOne({
            where:{
                id
            }
        })
        res.status(200).json(data)
    }

    static create = async (req, res, next) =>{
        const {agenda, location, status} = req.body;
        const data = await Todo.create({
            agenda,
            location,
            status
        })
        res.status(200).json(data)
    }
    static destroy = async (req, res, next) =>{

    }
}


module.exports = todoController;