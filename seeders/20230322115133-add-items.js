'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Todos', [
      {
        agenda: "mandi",
        status: "done",
        location: "rumah",
        createdAt: new Date(),
        updatedAt: new Date()

      },
      {
        agenda: "kerja",
        status: "on going",
        location: "kantor",
        createdAt: new Date(),
        updatedAt: new Date()

      },
      {
        agenda: "Nongkrong",
        status: "hold",
        location: "Cafe",
        createdAt: new Date(),
        updatedAt: new Date()

      },
      {
        agenda: "makan",
        status: "done",
        location: "rumah",
        createdAt: new Date(),
        updatedAt: new Date()

      },
  ])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
