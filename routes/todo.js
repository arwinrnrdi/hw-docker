const express = require ("express");
const router = express.Router();
const todoController = require("../controllers/todoController");
const todo = require("../models/todo");

router.get("/todo", todoController.findAll);
router.get("/todo/:id", todoController.findOne)
router.post("/todo", todoController.create)
module.exports = router;